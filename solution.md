# Solution

### 1.generate_password(length: int, complexity: int) -> str:

This function generate a password made by combination of lower-case letter,
upper-case letter and digits.
I first deal with the first complexity that all chars in password are lower-case letter.
Then, wirte a in-function helper function sub(string, int, int) aims to replace ith char
in string. By using this helper function, we can easily change a lower-case password to
password with at least one digits so that we can get complexity 2 password .(By my 
understanding, it does not require at least one lower-case letter in complexity 2.)
To generate complexity 3, we can call this function to build complexity 2 password which
has less length, then insert upper-case letter.
Complexity 4 is the same, generate complexity 3 first and then insert

### 2.check_password_level(password: str) -> int:
This function is easy if we improt re. iterate all chars in password to check if
it contains lower-case letter/upper-case letter/punctuation.
After that, check the length to enhance complexity for some password.

### 3.create_user(db_path: str) -> None:
This function is used to fentch data from websites and add them into database.
Firstly, use requests modular to get from https://randomuser.me/api/.
Secondly, seperate data and save in json structure.
Finally, embed SQL command to build/add into table.