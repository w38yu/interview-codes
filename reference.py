"""
    There are a few libs imported here
    but  their use is only optional
    you may import other libs as well
"""

import random  # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
import string  # https://docs.python.org/3.6/library/string.html
import requests
import json
import sqlite3
import re

def generate_password(length: int, complexity: int) -> str:
    """Generate a random password with given length and complexity
    
    Complexity levels:
        Complexity == 1: return a password with only lowercase chars
        Complexity ==  2: Previous level plus at least 1 digit
        Complexity ==  3: Previous levels plus at least 1 uppercase char
        Complexity ==  4: Previous levels plus at least 1 punctuation char

    :param length: number of characters
    :param complexity: complexity level
    :returns: generated password
    """
    pw = ""
    lower_list = "a b c d e f g h i j k l m n o p q r s t u v w x y z".split()
    upper_list = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z".split()
    number_list = "1 2 3 4 5 6 7 8 9 0".split()
    punc_list = "! # $ % & ' ( ) * + , - . / : ; < = > ? @ [ \ ] ^ _ ` { | } ~".split()
    # generate all lowercase
    def lowGerate(length: int):
        count = 0
        password = ""
        while count < length:
            password = password + lower_list[random.randint(0, len(lower_list) - 1)]
            count = count + 1
        return password


    # replace pth char to c
    def sub(string,p,c):
        new = []
        for s in string:
            new.append(s)
        new[p] = c
        return ''.join(new)

    def addPos(string,p,c):
        new = []
        for s in string:
            new.append(s)
        new.insert(p,c)
        return ''.join(new)
    #generate by levels
    if complexity == 1:
        pw = lowGerate(length)

    # NOTIC: I assume level 2 can all int(no lowercase)
    elif complexity == 2:
        pw = lowGerate(length)
        num = random.randint(1, length)
        for i in range(0,num):
            digit = number_list[random.randint(0, len(number_list) - 1)]
            pos = random.randint(0, length - 1)
            pw = sub(pw,pos,str(digit))

    elif complexity == 3:
        #has at least one dig
        num = random.randint(1, length-1)
        pw = generate_password(length-num, 2)
        for i in range(0,num):
            Up = upper_list[random.randint(0, len(upper_list) - 1)]
            pos = random.randint(0, len(pw)-1)
            pw = addPos(pw,pos,str(Up))

    #default to be complexity 4
    else:
        #has at least one dig and Uppercase
        num = random.randint(1, length-2)
        pw = generate_password(length-num, 3)
        for i in range(0,num):
            pun = punc_list[random.randint(0, len(punc_list) - 1)]
            pos = random.randint(0, len(pw)-1)
            pw = addPos(pw,pos,str(pun))
    return pw


def check_password_level(password: str) -> int:
    """Return the password complexity level for a given password

    Complexity levels:
        Return complexity 1: If password has only lowercase chars
        Return complexity 2: Previous level condition and at least 1 digit
        Return complexity 3: Previous levels condition and at least 1 uppercase char
        Return complexity 4: Previous levels condition and at least 1 punctuation

    Complexity level exceptions (override previous results):
        Return complexity 2: password has length >= 8 chars and only lowercase chars
        Return complexity 3: password has length >= 8 chars and only lowercase and digits

    :param password: password
    :returns: complexity level
    """
    level = 0
    if re.search('\s',password) is not None:
        print('The password include space') 
    if re.search('[a-z]',password) is not None:
        level = 1
    if re.search('[0-9]',password) is not None:
        level = 2
    if re.search('[A-Z]',password) is not None:
        level = 3
    if re.search('\W',password) is not None:
        level = 4

    if 1 == level and len(pw) >= 8:
        level = 2
    elif 2 == level and len(pw) >= 8:
        level = 3

    return level




def create_user(db_path: str) -> None:  # you may want to use: http://docs.python-requests.org/en/master/
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db

    :param db_path: path of the SQLite db file (to do: sqlite3.connect(db_path))
    :return: None
    """
    
    #random_usr = "".join(choice(allchar) for x in range(randint(4, 12)))
    r = requests.get("https://randomuser.me/api/")
    if r.status_code == 200:
        email = r.json()['results'][0]['email']
        firstname = r.json()['results'][0]['name']['first']
        lastname = r.json()['results'][0]['name']['last']
        #username = r.json()['results'][0]['login']['username']
        
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    table_name = 'table_name'
    sql = 'create table if not exists ' + table_name + ' (id INT AUTO_INCREMENT PRIMARY KEY, firstname VARCHAR(255), lastname VARCHAR(255), email VARCHAR(255))'
    c.execute(sql)
    conn.commit()
    
    insert_sql = 'insert into ' + table_name + ' VALUES (%s, %s, %s)', (firstname, lastname, email) 
    c.execute(insert_sql)
    conn.commit()



